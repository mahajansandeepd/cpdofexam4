This is a sample Web Application to use during Continuous Integration demos.

#War file creation Instruction

```
mvn clean install
```
# Will result in CPDOFWebApp.war creation

#For the exam you have to submit the following
1. Screen shots of three jenkins jobs both the latest build and the main job screenshot
a. PMD
b. Cobertura
c. Deployment to local tomcat and docker
d. Console outputlog for the last successful or unsuccessful jobs
